import React, { useEffect, useRef, useState } from "react";
import {Icon} from 'native-base';
import { StatusBar,ActivityIndicator,BackHandler,Linking,RefreshControl,SafeAreaView,ImageBackground,TouchableHighlight,View,StyleSheet,Dimensions,Image,Text,TouchableOpacity,FlatList,ScrollView,Alert, Platform} from 'react-native';
import {GetAll,GetStatistic,CreateGameHistory} from './ReportData';
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.90)
const SLIDER_WIDTH = Dimensions.get('window').width 
const screen_width = Dimensions.get('screen').width;
const screen_height = Dimensions.get('screen').height;
let Clear = ["", "", "", "", "", "", "", "", ""];
const App = () => {
  const [boardArrat,setBoardArrat]= useState(["", "", "", "", "", "", "", "", ""]);
  const [count,setcount]= useState(0);
  const [result,setResult]= useState("");
  const winCombos = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [6, 4, 2]
  ];

  useEffect(() => {
    async function FetchAll() {
      //GetStatistic();
      GetAll();
    }
    FetchAll();
  }, [])



  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1 }}>
      <View style={styles.StyRow}>
       <Text style={styles.StyResult}>
              {result}
       </Text>
      </View>
      <View style={styles.StyRow}>
        <View style={styles.button}>
        <View style={{flex: 1}}>

        </View>
        <View style={{flex: 1}}>
          
        </View>
            <TouchableOpacity
              onPress={() => {
                //ส่งฟังก์ชั่นไป api หลังบ้าน
                setBoardArrat(Clear);
                setResult("");
              }}>
             
             <Text
                    style={[
                      styles.textSign,
                      {
                        color: colors.textDark,
                      },
                    ]}>
                  New Game
                </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                //ส่งฟังก์ชั่นไป api หลังบ้าน
                setBoardArrat(Clear);
                setResult("");
              }
              }
              >
             
                <Text
                    style={[
                      styles.btnEndGame,
                      {
                        color: colors.textDark,
                      },
                    ]}>
                  End Game
                </Text>
            </TouchableOpacity>
          </View>


        </View>
       <FlatList
            numColumns={3}
            data={boardArrat}
            renderItem={({item,index}) => {
              return (
                <View style={styles.mainManuWidthItem}>
                <TouchableOpacity
                  onPress={
                    () => {
                      let newArr = [...boardArrat];
                      if(count%2===0){
                        item="O";
                      }
                      else{
                        item="X";
                      }
                      setcount(count+1)
                      newArr[index] = item;
                      setBoardArrat(newArr);
                      for (let userObject of winCombos) {
                        if(newArr[userObject[0]] === "O" && newArr[userObject[1]] === "O" && newArr[userObject[2]] === "O"){
                          console.log("O WIN");
                          setResult("O WIN");
                          setBoardArrat(Clear);
                        }
                        else if(newArr[userObject[0]] === "X"&& newArr[userObject[1]] === "X"&& newArr[userObject[2]] === "X"){
                          console.log("X WIN");
                          setResult("X WIN");
                          setBoardArrat(Clear);
                        }
                        
                      }
                    }
                  }
                  style={[
                    styles.TabMainMenu,
                    {
                      color: "#19454e",
                      backgroundColor:  '#ffffff',
                    },
                  ]}>
                  <Text
                    numberOfLines={1}
                    style={[
                      {
                        fontSize: 50,
                        color: "#767676",
                        marginTop:5
                      }
                    ]}>
                    {item}
                  </Text>
                 
                </TouchableOpacity>


               
              </View>
              );
            }}
          />
        
        </View>
    </SafeAreaView>
  );
};




const styles = StyleSheet.create({
  StyRow: {
    marginTop:10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
   },
  StyResult: {
    fontSize:50,
    color:"#19454e",
    justifyContent: 'center',
    alignItems: 'center',
},
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    flex: 1,
  },
  button: {
    alignItems: 'center',
    marginTop: 20,
  },
  btnEndGame: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  mainManuWidthItem: {
    marginRight:3,
    marginBottom:5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TabMainMenu: {
    width: screen_width * 0.28,
    height: screen_height * 0.1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopEndRadius: screen_width * 0.03,
    borderTopStartRadius: screen_width * 0.03,
    borderBottomEndRadius: screen_width * 0.03,
    borderBottomLeftRadius: screen_width * 0.03,
    borderWidth:1,
    borderColor:"#C3C3C3",
    margin:5,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 1.84,
    elevation: 5,
  },












});

const colors = {
  primary: '#D0103A',
//kfprimary:"#262f3e",
  kfprimary:"#19454e",
  secondary: "#404d5e",
  thirdary: "#a40c2d",
  thirdary2: "#a40c2d",
   // thirdary: "#ea9e4a",
  fourthary: "#f9f9fb",
  fivethary: "#ffffff",
  medium2: "#767676",
  medium3: "#dddddd",
  medium4: "#F0F0F0",
  black: "#000",
  textDark: "#19454e",
  textDark2: "#221F20",
  textLight: '#f8f9f8',
  textholder: '#666666',
  textholder2: "#adadad",
  forground: '#275ca5',
  forground2: '#f2b80e',
  forground3: '#f2f2f2',
  forground4: '#44a6a6',
  background: "#19454e",
  background1: '#07C7B7',
  background2: '#d61503',
  white: '#ffffff',
  light: "#f8f4f4",
  medium: "#6e6969",
  mediumBackgroud: "#DFDCDC",
  mediumBackgroudChat: "#f0f0f0",
  mediumChatTime: "#adadad",
  backgroundChatOwner: '#19454e',
  backgroundChatAdmin: '#ffffff',
}
export default App;
